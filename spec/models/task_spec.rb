require 'spec_helper'

describe Task do
  describe "validations" do
    it "presence title" do
      task = build(:task, title: '')
      task.valid?.should be_false

      task.title = "learn rspec"
      task.valid?.should be_true
    end

    it "presence user" do
      task = build(:task, user_id: nil)
      task.valid?.should be_false

      task.user = create(:user)
      task.valid?.should be_true
    end
  end

  it 'close task' do
    task = create(:task, closed: false)
    task.closed?.should eq false
    task.close
    task.closed?.should eq true
  end

  it 'reopen task' do
    task = create(:task, closed: true)
    task.closed?.should eq true
    task.reopen
    task.closed?.should eq false
  end
end
