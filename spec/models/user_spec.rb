require 'spec_helper'

describe User do
  before do
    @user = create(:user)
  end

  it 'is valid' do
    @user.valid?.should eq true
  end

  describe 'validations' do
    it 'presence of name' do
      user = build(:user, name: '')
      user.valid?.should be_false

      user.name = 'Charles'
      user.valid?.should be_true
    end

    it 'maximum length of name' do
      user = build(:user, name: 'T'*51)
      user.valid?.should be_false

      user.name = 'Charles'
      user.valid?.should be_true
    end

    it 'presence of surname' do
      user = build(:user, surname: '')
      user.valid?.should be_false

      user.surname = 'Darwin'
      user.valid?.should be_true
    end

    it 'maximum length of surname' do
      user = build(:user, surname: 'T'*151)
      user.valid?.should be_false

      user.surname = 'Darwin'
      user.valid?.should be_true
    end
  end

  describe 'associations' do
    describe 'has_many tasks' do
      before do
        3.times{ @user.tasks << create(:task, user_id: @user.id)}
      end

      it 'add and access to tasks' do
        @user.tasks.count.should eq 3
      end

      it 'dependent destroy' do
        @user.tasks.count.should eq 3

        user_id = @user.id
        @user.destroy
        Task.where(user_id: user_id).count.should eq 0
      end
    end
  end

  it 'should return full name' do
    green_user = create(:user, name: 'Bruce', surname: 'Banner')
    green_user.full_name.should eq 'Bruce Banner'
  end
end
