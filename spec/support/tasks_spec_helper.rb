module TasksSpecHelper
  def visit_user_tasks(user)
    visit users_path
    within "#user_#{user.id}" do
      click_link 'tasks'
    end
    current_path.should eq user_tasks_path(user)
  end
end
