require 'spec_helper'

describe TasksController do

  describe "json format" do
    before do
      @user = create(:user)
    end

    describe "#index" do
      before do
        3.times { create(:task, user_id: @user.id)}
      end

      it "get all users" do
        get :index, user_id: @user.id, format: :json
        response.should be_success

        JSON.parse(response.body).size.should eq 3
      end
    end

    describe "#show" do
      before do
        @task = create(:task, user_id: @user.id)
      end

      it "get task" do
        get :show, user_id: @user.id, id: @task.id, format: :json
        response.should be_success

        JSON.parse(response.body)["title"].should eq @task.title
        JSON.parse(response.body)["owner"].should eq @user.full_name
      end
    end

    describe "#close" do
      before do
        @task = create(:task, user_id: @user.id, closed: false)
      end

      it "close task" do
        put :close, user_id: @user.id, id: @task.id, format: :json
        response.should be_success
      end
    end

    describe "#reopen" do
      before do
        @task = create(:task, user_id: @user.id, closed: true)
      end

      it "reopen task" do
        put :reopen, user_id: @user.id, id: @task.id, format: :json
        response.should be_success
      end
    end
  end

end
