require 'spec_helper'

describe UsersController do
  describe "json format" do
    describe "#index" do
      before do
        3.times{create(:user)}
      end

      it "get all users" do
        get :index, format: :json
        response.should be_success

        JSON.parse(response.body).size.should eq 3
      end
    end

    describe "#show" do
      before do
        @user = create(:user, name: 'Matthew Michael', surname: 'Murdock')
      end

      it "get user" do
        get :show, id: @user.id, format: :json
        response.should be_success

        JSON.parse(response.body)["name"].should eq @user.name
      end
    end
  end
end
