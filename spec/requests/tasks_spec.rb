require 'spec_helper'

describe "Tasks" do
  before do
    @user = create(:user)
  end

  describe "GET /users/:user_id/tasks" do
    before do
      @task = create(:task)
    end

    it "list user tasks" do
      visit_user_tasks(@user)
      page.should have_content("#{@user.full_name} Tasks")
    end
  end

  describe "POST /users/:user_id/task" do
    it "add taks to user tasks list" do
      visit_user_tasks(@user)
      within '#new_task' do
        click_button 'Create Task'
      end
      page.should have_content("Task can't be added")
      within '#new_task' do
        fill_in 'task_title', with: 'Refactor my code'
        click_button 'Create Task'
      end
      current_path.should eq user_tasks_path(@user)
      page.should have_content('Task added')
      page.should have_content('Refactor my code')
    end
  end

  describe "GET /users/:user_id/tasks/:id/edit" do
    before do
      @task = create(:task)
    end

    it "display edit form" do
      visit_user_tasks(@task.user)
      within "#task_#{@task.id}" do
        click_link 'edit'
      end
      current_path.should eq edit_user_task_path(user_id: @task.user_id, id: @task.id)
      page.should have_content('Edit Task')
    end
  end

  describe "PUT /users/:user_id/tasks/:id" do
    before do
      @task = create(:task)
    end

    it "update task" do
      visit_user_tasks(@task.user)
      within "#task_#{@task.id}" do
        click_link 'edit'
      end
      current_path.should eq edit_user_task_path(user_id: @task.user_id, id: @task.id)
      within "#edit_task_#{@task.id}" do
        fill_in 'task_title', with: ''
        click_button 'Update Task'
      end
      page.should have_content("Title can't be blank")

      within "#edit_task_#{@task.id}" do
        fill_in 'task_title', with: 'Learn Rspec'
        fill_in 'task_description', with: 'Learn a new way to test ruby applications'
        click_button 'Update Task'
      end
      current_path.should eq user_tasks_path(@task.user)
      page.should have_content('Task updated')
      page.should have_content('Learn Rspec')
    end
  end

  describe "DELETE /users/:user_id/tasks/:id" do
    before do
      @task = create(:task)
    end

    it 'delete task' do
      visit_user_tasks(@task.user)
      within "#task_#{@task.id}" do
        click_link 'delete'
      end
      current_path.should eq user_tasks_path(@task.user)
      page.should have_content('Task deleted')
    end
  end

  describe "PUT /users/:user_id/tasks/:id/close" do
    before do
      @task = create(:task)
    end

    it 'close task' do
      visit_user_tasks(@task.user)
      within "#task_#{@task.id}" do
        click_link 'close'
      end
      current_path.should eq user_tasks_path(@task.user)
      page.should have_content('Task closed')
    end
  end

  describe "PUT /users/:user_id/tasks/:id/reopen" do
    before do
      @task = create(:task, closed: true)
    end

    it 'close task' do
      visit_user_tasks(@task.user)
      within "#task_#{@task.id}" do
        click_link 'reopen'
      end
      current_path.should eq user_tasks_path(@task.user)
      page.should have_content('Task has been reopened')
    end
  end
end
