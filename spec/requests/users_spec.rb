require 'spec_helper'

describe "Users" do
  describe "GET /users" do
    before(:each) do
      create(:user, name: 'Bruce', surname: 'Wayne')
      create(:user, name: 'Peter', surname: 'Parker')
    end

    it "access to users list" do
      visit users_path
      page.should have_content("Users List")
    end

    it "list all users" do
      visit users_path
      within '#users' do
        page.should have_content('Peter Parker')
      end
    end
  end

  describe 'GET /users/new' do
    it 'access from index' do
      visit users_path
      click_link 'new_user'
      current_path.should eq new_user_path
    end
  end

  describe 'POST /users' do
    it 'create new user' do
      visit new_user_path
      within '#new_user' do
        fill_in 'user_surname', with: 'Octavius'
        click_button 'Create User'
      end
      page.should have_content("Name can't be blank")
      within '#new_user' do
        fill_in 'user_name', with: 'Clark'
        fill_in 'user_surname', with: 'Kent'
        click_button 'Create User'
      end
      current_path.should eq users_path
      page.should have_content('New user has been created')
    end
  end

  describe 'GET /users/:id/edit' do
    before do
      @user = create(:user, name: 'Tonny', surname: 'Stark')
    end

    it 'user edit form' do
      visit users_path
      within "#user_#{@user.id}" do
        click_link "edit"
      end
      current_path.should eq edit_user_path(@user)
      page.should have_content('Tonny Stark')
    end
  end

  describe 'PUT /users/:id' do
    before do
      @user = create(:user, name: 'Tonny', surname: 'Stark')
    end

    it 'update user' do
      visit users_path
      within "#user_#{@user.id}" do
        click_link "edit"
      end
      current_path.should eq edit_user_path(@user)
      within "#edit_user_#{@user.id}" do
        fill_in 'user_name', with: ''
        click_button 'Update User'
      end
      page.should have_content("Name can't be blank")
      within "#edit_user_#{@user.id}" do
        fill_in 'user_name', with: 'Tony'
        click_button 'Update User'
      end
      current_path.should eq users_path
      page.should have_content('Tony Stark')
    end
  end

  describe 'DELETE /users/:id' do
    before do
      @user = create(:user, name: 'Nick', surname: 'Fury')
    end

    it 'destroy user' do
      visit users_path
      page.should have_content('Nick Fury')
      within "#user_#{@user.id}" do
        click_link 'delete'
      end
      current_path.should eq users_path
      page.should_not have_content('Nick Fury')
    end
  end
end
