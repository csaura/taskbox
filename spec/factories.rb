FactoryGirl.define do
  factory :user do
    name 'Charles'
    surname  'Darwin'
  end

  factory :task do
    title 'Do homework'
    description  'create some app with testing...'
    closed false
    user { |c| c.association(:user) }
  end
end
