object @task
attributes :id, :title, :description, :closed

node(:owner) {|task| task.user.full_name }
