class TasksController < ApplicationController
  respond_to :html, :json

  def index
    @user = get_user(params[:user_id])
  end

  def show
    @task = get_task(params[:id])
  end

  def create
    user = get_user(params[:user_id])
    task = user.tasks.new(params[:task])

    if task.save
      flash[:notice] = "Task added"
    else
      flash[:error] = "Task can't be added"
    end
    redirect_to user_tasks_path(user)
  end

  def edit
    @task = get_task(params[:id])
  end

  def update
    @task = get_task(params[:id])

    if @task.update_attributes(params[:task])
      redirect_to user_tasks_path(@task.user), notice: "Task updated"
    else
      render :edit
    end
  end

  def destroy
    task = get_task(params[:id])
    task.destroy
    redirect_to user_tasks_path(task.user), notice: "Task deleted"
  end

  def close
    task = get_task(params[:id])
    task.close
    respond_to do |format|
      format.html { redirect_to user_tasks_path(task.user), notice: "Task closed" }
      format.json { head(:ok) }
    end
  end

  def reopen
    task = get_task(params[:id])
    task.reopen

    respond_to do |format|
      format.html {redirect_to user_tasks_path(task.user), notice: "Task has been reopened"}
      format.json { head(:ok) }
    end
  end

  private
  def get_user(user_id)
    User.find(user_id, include: :tasks)
  end

  def get_task(task_id)
    Task.find(params[:id], include: :user)
  end
end
