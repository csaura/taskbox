class UsersController < ApplicationController
  respond_to :html, :json

  def index
    @users = User.all
    respond_with(@users)
  end

  def show
    @user = get_user(params[:id])
    respond_with(@user)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(params[:user])
    if @user.save
      redirect_to users_path, notice: 'New user has been created'
    else
      render :new
    end
  end

  def edit
    @user = get_user(params[:id])
  end

  def update
    @user = get_user(params[:id])

    if @user.update_attributes(params[:user])
      redirect_to users_path, notice: 'The user has been updated'
    else
      render :edit
    end
  end

  def destroy
    @user = get_user(params[:id])
    @user.destroy

    redirect_to users_path, notice: 'The user has been deleted'
  end

  private
  def get_user(user_id)
    User.find(user_id)
  end
end
