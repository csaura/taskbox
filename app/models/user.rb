class User < ActiveRecord::Base
  has_many :tasks, dependent: :destroy
  attr_accessible :name, :surname

  validates :name, presence: true, length: {maximum: 50}
  validates :surname, presence: true, length: {maximum: 150}

  def full_name
    "#{name} #{surname}"
  end
end
