class Task < ActiveRecord::Base
  belongs_to :user
  attr_accessible :description, :title

  validates :title, :user_id, presence: true

  def close
    update_attribute(:closed, true)
  end

  def reopen
    update_attribute(:closed, false)
  end
end
