class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title, default: "", null: false
      t.text :description, default: ""
      t.references :user, null: false

      t.timestamps
    end
    add_index :tasks, :user_id
  end
end
