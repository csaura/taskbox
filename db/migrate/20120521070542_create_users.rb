class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, limit: 50, default: "", null: false
      t.string :surname, limit: 150, default: "", null: false

      t.timestamps
    end
  end
end
